package Controlador.TDA_Grafos;

import Controlador.Exceptions.PosicionException;
import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Lista.ListaEnlazada;

/**
 *
 * @author Jordy
 */
public class GrafoD extends Grafo {

    protected Integer numV;
    protected Integer numA;
    protected ListaEnlazada<Adyacencia> listaAdyacente[];

    public GrafoD(Integer numV) {
        this.numV = numV;
        this.numA = 0;
        listaAdyacente = new ListaEnlazada[numV + 1];
        for (int i = 0; i <= this.numV; i++) {
            listaAdyacente[i] = new ListaEnlazada<>();
        }

    }

    public void comprobasF() {
        for (int i = 1; i <= numV; i++) {
            System.out.println(listaAdyacente[i].getSize());
        }
        System.out.println("---");
    }

    @Override
    public Integer numVertices() {
        return this.numV;
    }

    @Override
    public Integer numAristas() {
        return this.numA;
    }

    /**
     * Permite verificar si existe una conexion entre aristas
     *
     * @param i vertice inicial
     * @param f vertice final
     * @return arreglo de objetos: en la posicion 0 regresa un boolean y en la 1
     * el peso
     * @throws VerticeException
     */
    @Override
    public Object[] existeArista(Integer i, Integer f) throws VerticeException {
        Object[] resultado = {Boolean.FALSE, Double.NaN};
        if (i > 0 && f > 0 && i <= numV && f <= numV) {
            ListaEnlazada<Adyacencia> lista = listaAdyacente[i];
            for (int j = 0; j < lista.getSize(); j++) {
                try {
                    Adyacencia aux = lista.obtenerDato(j);
                    if (aux.getDestino().intValue() == f.intValue()) {
                        resultado[0] = Boolean.TRUE;
                        resultado[1] = aux.getPeso();
                        break;
                    }
                } catch (PosicionException ex) {
                    //       System.out.println(ex);
                }
            }
        } else {
            //throw new VerticeException("Error en existeArista :Algun vertice ingresado no existe");
        }
        return resultado;
    }

    @Override
    public Double pesoArista(Integer i, Integer f) throws VerticeException {
        Double peso = Double.NaN;
        Object[] existe = existeArista(i, f);
        if (((Boolean) existe[0])) {
            peso = (Double) existe[1];
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer i, Integer j) throws VerticeException {
        insertarArista(i, j, Double.NaN);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 && i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
            }
        } else {
            throw new VerticeException("Error en insertar arista: Algun vertice ingresado no existe");
        }

    }

    @Override
    public ListaEnlazada<Adyacencia> adyacente(Integer i) throws VerticeException {
        return listaAdyacente[i];
    }

//    public static void main(String[] args) {
//        GrafoD grafo = new GrafoD(6);
//        
//        try {
//            grafo.insertarArista(2, 5, 10.00);
//            System.out.println(grafo.existeArista(2, 5)[0]);
//            System.out.println(grafo.toString());
//        } catch (Exception e) {
//            System.out.println("ERROR: " +e);
//        }
//    }
    protected Integer[] visitados;
    protected Integer ordenVisita;
    protected ListaEnlazada<Integer> q;

    //Busqueda en profundidad
    public Integer[] toArrayDFS() throws Exception {
        Integer res[] = new Integer[numVertices() + 1];
        visitados = new Integer[numVertices() + 1];
        ordenVisita = 0;
        for (int i = 0; i <= numVertices(); i++) {
            visitados[i] = 0;
        }

        for (int i = 0; i <= numVertices(); i++) {
            if (visitados[i] == 0) {
                toArrayDFS(i, res);
            }
        }
        return res;
    }

    public void toArrayDFS(Integer origen, Integer res[]) throws Exception {
        res[ordenVisita] = origen;
        visitados[origen] = ordenVisita++;
        ListaEnlazada<Adyacencia> lista = listaAdyacente[origen];
        for (int i = 0; i < lista.getSize() - 1; i++) {
//            System.out.println(lista.obtenerDato(i));
            Adyacencia a = lista.obtenerDato(i);
            lista.setCabecera(lista.getCabecera().getSiguiente());
            if (visitados[a.getDestino()] == 0) {
                toArrayDFS(a.getDestino(), res);
            }
        }
    }

    public String toStringDFS() throws Exception {
        return arrayToString(toArrayDFS());
    }

    public String arrayToString(Integer v[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < v.length; i++) {
            sb.append(v[i] + "\n");
        }
        return sb.toString();
    }

    //Busqueda en anchura
    public String toStringBFS() {
        try {
            return arrayToString(toArrayBFS());
        } catch (Exception ex) {
            return null;
        }
    }

    public Integer[] toArrayBFS() throws Exception {
        Integer res[] = new Integer[numVertices() + 1];
        visitados = new Integer[numVertices() + 1];
        for (int i = 0; i <= numVertices(); i++) {
            visitados[i] = 0;

        }
        ordenVisita = 0;
        q = new ListaEnlazada<>();

        for (int i = 0; i < numVertices(); i++) {
            for (int j = 0; j <= numVertices(); j++) {
                if (visitados[i] == 0) {
                    toArrayBFS(i, res);
                }
            }
        }

        return res;
    }

    public Integer[] BPA() throws Exception {
        Integer aux[] = toArrayBFS();
        Integer aux1[] = aux;
        for (int i = 1; i < aux.length - 1; i++) {
            aux[i] = aux1[i + 1];
        }
        aux[aux.length - 1] = aux.length - 1;
        return aux;
    }

    public void toArrayBFS(int origen, Integer res[]) throws PosicionException, VerticeException {
        res[ordenVisita] = origen;
        visitados[origen] = ordenVisita++;
        q.insertarCabecera(origen);
        while (!q.estaVacia()) {
            Integer u = q.eliminarDato(0).intValue();
            ListaEnlazada<Adyacencia> lista = listaAdyacente[u];
            for (int i = 0; i < lista.getSize() - 1; i++) {
                Adyacencia a = lista.obtenerDato(i);
                if (visitados[a.getDestino()] == 0) {
                    res[ordenVisita] = a.getDestino();
                    visitados[a.getDestino()] = ordenVisita++;
                    q.insertarCabecera(a.getDestino());
                }
            }
        }
    }

    public Integer[] BPA(Integer inicio) throws PosicionException {
        Integer[] recorrido = new Integer[numV];
        Integer cont = 0;
        for (int i = 0; i < numV; i++) {

            for (int j = 0; j < listaAdyacente[inicio].getSize(); j++) {
recorrido[i] =listaAdyacente[inicio].obtenerDato(j).getDestino();
                cont++;
            }
            if (inicio == numV) {
                inicio = 1;
            }
            if (cont == numV) {
                break;
            }
            inicio++;
        }
        return recorrido;
    }
}
