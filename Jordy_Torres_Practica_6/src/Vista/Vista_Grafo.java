package Vista;

import Controlador.Vista.FrmGrafoVista;
import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Grafos.Adyacencia;
import Controlador.TDA_Grafos.GrafoD;
import Controlador.TDA_Grafos.GrafoDE;
import Controlador.TDA_Grafos.GrafoEND;
import Controlador.TDA_Grafos.GrafoND;
import Controlador.TDA_Lista.ListaEnlazada;
import Controlador.Vista.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jordy
 */
public class Vista_Grafo extends javax.swing.JFrame {

    private GrafoDE gd = new GrafoDE(0, String.class);
    private ModeloTablaGrafo tgd = new ModeloTablaGrafo();
    private boolean isDE = true;
    private boolean[] esAnchura = new boolean[gd.numVertices()]; 
    ListaEnlazada<Adyacencia> listaAdyacente = new ListaEnlazada<>();
    public Vista_Grafo() {
        this.setLocationRelativeTo(null);
        initComponents();
        jPanel4.setVisible(true);
        jPanel3.setVisible(true);
        jPanel2.setVisible(true);
    }

    private void generarGrafo() throws Exception {

        if (!String.valueOf(this.nombreVertice.getText()).equalsIgnoreCase("") && -1 == gd.obtenerCodigo(String.valueOf(this.nombreVertice.getText()))) {
             GrafoDE aux = gd;
 
                if(OpcG.getSelectedIndex()==0){
                    isDE = true;
                    this.gd = new GrafoEND(aux.numVertices() + 1, String.class);
                    for (int i = 1; i <= aux.numVertices(); i++) {
                        gd.etiquetarVertice(i, aux.obtenerEtiqueta(i));
                     }
                    for (int i = 1; i <= aux.numVertices(); i++) {
                        for (int j = 0; j < aux.adyacente(i).getSize(); j++) {
                            gd.insertarAristaE(aux.obtenerEtiqueta(i), aux.obtenerEtiqueta(aux.adyacente(i).obtenerDato(j).getDestino()), (Double) aux.existeAristaE(aux.obtenerEtiqueta(i), aux.obtenerEtiqueta(aux.adyacente(i).obtenerDato(j).getDestino()))[1]);
                        }
                    }
                    gd.etiquetarVertice(gd.numVertices(), String.valueOf(this.nombreVertice.getText()));

                }
           
        } else {
            JOptionPane.showMessageDialog(null, "Error con nombre", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
 
        cbxFin.removeAllItems();
        cbxOrigen1.removeAllItems();
        cbxDestino.removeAllItems();
      cbxinicio.removeAllItems();
        for (int i = 1; i <= gd.numVertices(); i++) {
            cbxFin.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cbxOrigen1.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cbxDestino.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cbxinicio.addItem(String.valueOf(gd.obtenerEtiqueta(i)));
            cargarTabla();
              this.nombreVertice.setText(null);
        }
    }
    public ListaEnlazada<Integer> recorridoAnchura(Integer nodoI) throws Exception{
        ListaEnlazada<Integer> recorrido = new ListaEnlazada<>();
        esAnchura[nodoI]=true;
        ListaEnlazada<Integer> lista = new ListaEnlazada<>();
        recorrido.insertarCabecera(nodoI);
        lista.insertarCabecera(nodoI);
        while (!lista.estaVacia()) {            
            //int c = lista.eliminarDato(0);
            for (int i = 0; i < gd.numVertices(); i++) {
                if (listaAdyacente.obtenerDato(i).equals(1)&&!esAnchura[i]) {
                    lista.insertarCabecera(i);
                    recorrido.insertarCabecera(i);
                    esAnchura[i]=true;
                }
            }
        }
        return recorrido;
    }

    public DefaultTableModel getTableListaDis(Boolean todosDatos) throws Exception {
        ListaEnlazada listaDis[] = gd.dijkStra(String.valueOf(cbxinicio.getSelectedItem().toString()), String.valueOf(cbxFin.getSelectedItem().toString()), todosDatos);
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Vertice llegada");
        model.addColumn("Peso");
        model.addColumn("Recorrido");
        String datos[] = new String[3];
        for (int i = 0; i < listaDis.length; i++) {
            datos[0] = String.valueOf(listaDis[i].obtenerDato(0));
            datos[1] = String.valueOf(listaDis[i].obtenerDato(1));
            datos[2] = String.valueOf(listaDis[i].obtenerDato(2));
            model.addRow(datos);
        }
        return model;
    }

    private void cargarTabla() throws Exception {
        tgd.setGrafoED(gd);
        tblTabla.setModel(tgd);
        tgd.fireTableStructureChanged();
        tblTabla.updateUI();
    }

    private void adyacencias() throws Exception {
        String i = String.valueOf(cbxOrigen1.getSelectedItem().toString());
        String j = String.valueOf(cbxDestino.getSelectedItem().toString());

        if (i.equalsIgnoreCase(j)) {
            JOptionPane.showMessageDialog(null, "Vertices iguales", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                gd.insertarAristaE(i, j, Double.parseDouble(this.peso.getText()));
             //   System.out.println(recorridoAnchura(Integer.parseInt(peso.getText())));
                cargarTabla();
            } catch (VerticeException ex) {
                System.out.println("ERROR EN INSERTAR ADYACENCIA: " + ex);
                JOptionPane.showMessageDialog(null, "No se puede insertar", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
         this.peso.setText(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TipoGrafo = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbxDestino = new javax.swing.JComboBox<>();
        peso = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cbxOrigen1 = new javax.swing.JComboBox<>();
        Unir = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnMostrar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        btnModificar = new javax.swing.JButton();
        nombreVertice = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        OpcG = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        cbxFin = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        cbxinicio = new javax.swing.JComboBox<>();
        btnOk2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaDis = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Conectar"));
        jPanel3.setLayout(null);

        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Origen");
        jPanel3.add(jLabel2);
        jLabel2.setBounds(11, 33, 37, 16);

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Destino");
        jPanel3.add(jLabel3);
        jLabel3.setBounds(200, 29, 43, 16);

        cbxDestino.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));
        jPanel3.add(cbxDestino);
        cbxDestino.setBounds(293, 24, 66, 26);
        jPanel3.add(peso);
        peso.setBounds(160, 60, 88, 24);

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Peso o distancia:");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(50, 70, 95, 16);

        cbxOrigen1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));
        jPanel3.add(cbxOrigen1);
        cbxOrigen1.setBounds(110, 30, 80, 26);

        Unir.setBackground(new java.awt.Color(51, 51, 51));
        Unir.setForeground(new java.awt.Color(255, 255, 255));
        Unir.setText("Unir");
        Unir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UnirActionPerformed(evt);
            }
        });
        jPanel3.add(Unir);
        Unir.setBounds(267, 60, 92, 24);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(250, 20, 380, 130);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Conexiones"));
        jPanel4.setLayout(null);

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel4.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 670, 90);

        btnMostrar.setBackground(new java.awt.Color(51, 51, 51));
        btnMostrar.setForeground(new java.awt.Color(255, 255, 255));
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        jPanel4.add(btnMostrar);
        btnMostrar.setBounds(20, 120, 75, 24);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(20, 160, 700, 150);

        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnModificar.setText("Agregar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        jLabel7.setText("Etiqueta");

        OpcG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Grafo Dirigido" }));

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Tipo de grafo");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(71, 71, 71))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nombreVertice, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(OpcG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(63, 63, 63))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(OpcG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreVertice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModificar)
                .addGap(16, 16, 16))
        );

        jPanel1.add(jPanel5);
        jPanel5.setBounds(10, 30, 230, 120);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setLayout(null);

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("inicio");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(10, 20, 29, 16);

        cbxFin.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));
        jPanel2.add(cbxFin);
        cbxFin.setBounds(92, 48, 80, 26);

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Final");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(8, 53, 70, 16);

        cbxinicio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0" }));
        cbxinicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxinicioActionPerformed(evt);
            }
        });
        jPanel2.add(cbxinicio);
        cbxinicio.setBounds(90, 16, 80, 26);

        btnOk2.setBackground(new java.awt.Color(51, 51, 51));
        btnOk2.setForeground(new java.awt.Color(255, 255, 255));
        btnOk2.setText("Algoritmo Dijkstra");
        btnOk2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOk2ActionPerformed(evt);
            }
        });
        jPanel2.add(btnOk2);
        btnOk2.setBounds(190, 20, 130, 24);

        tablaDis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaDis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaDisMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaDis);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(330, 10, 370, 87);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(30, 320, 710, 110);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 750, 440);

        setSize(new java.awt.Dimension(768, 478));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        FrmGrafoVista frm = new FrmGrafoVista(gd);
        frm.setSize(400, 400);
        frm.setVisible(true);
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        try {
            this.generarGrafo();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error en grafos", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void UnirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UnirActionPerformed
        try {
            adyacencias();
        } catch (Exception ex) {
            Logger.getLogger(Vista_Grafo.class.getName()).log(Level.SEVERE, null, ex);
        }        // TODO add your handling code here:        // TODO add your handling code here:


    }//GEN-LAST:event_UnirActionPerformed

    private void tablaDisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaDisMouseClicked
        if (evt.getClickCount() == 2) {
            //Datos a mostrar en pantalla
            //    cargarVista();
        }
    }//GEN-LAST:event_tablaDisMouseClicked

    private void btnOk2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOk2ActionPerformed
       
        try {
           
            this.tablaDis.setModel(this.getTableListaDis(false));
        } catch (Exception ex) {
            Logger.getLogger(Vista_Grafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnOk2ActionPerformed

    private void cbxinicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxinicioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxinicioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vista_Grafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vista_Grafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vista_Grafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vista_Grafo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vista_Grafo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> OpcG;
    private javax.swing.ButtonGroup TipoGrafo;
    private javax.swing.JButton Unir;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnOk2;
    private javax.swing.JComboBox<String> cbxDestino;
    private javax.swing.JComboBox<String> cbxFin;
    private javax.swing.JComboBox<String> cbxOrigen1;
    private javax.swing.JComboBox<String> cbxinicio;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField nombreVertice;
    private javax.swing.JTextField peso;
    private javax.swing.JTable tablaDis;
    private javax.swing.JTable tblTabla;
    // End of variables declaration//GEN-END:variables
}
